import { GET_ALL_POSTS_SUCCESS } from "../actions/actionTypes";


const initialState = {
	posts: []
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case GET_ALL_POSTS_SUCCESS:
			return {...state, posts: action.posts};
		default:
			return state;
	}
};

export default reducer;