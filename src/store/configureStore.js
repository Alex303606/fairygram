import { applyMiddleware, compose, createStore } from "redux";
import thunkMiddleware from "redux-thunk";
import { persistCombineReducers, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { AsyncStorage } from 'react-native'
import userReducer from "./reducers/user";
import postsReducer from "./reducers/posts";
import navReducer from "./reducers/navigation";
import { navMiddleware } from "../utils/redux";

const middleware = [
	thunkMiddleware,
	navMiddleware
];

const persistConfig = {
	key: "root",
	storage,
	blacklist: ['nav']
};

const rootReducer = persistCombineReducers(persistConfig, {
	nav: navReducer,
	user: userReducer,
	posts: postsReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));


const store = createStore(rootReducer, enhancers);

persistStore(store);

export default store;