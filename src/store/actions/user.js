import axios from '../../axios-api';
import {
	LOGIN_USER_FAILURE,
	LOGIN_USER_SUCCESS,
	LOGOUT_USER,
	REGISTER_USER_FAILURE,
	REGISTER_USER_SUCCESS, USER_LOGGED_IN, USER_LOGGED_OUT
} from "./actionTypes";

const userLoggedIn = () => {
	return {type: USER_LOGGED_IN};
};

const userLoggedOut = () => {
	return {type: USER_LOGGED_OUT};
};

const registerUserSuccess = () => {
	return {type: REGISTER_USER_SUCCESS};
};

const registerUserFailure = error => {
	return {type: REGISTER_USER_FAILURE, error};
};

export const registerUser = userData => {
	return dispatch => {
		return axios.post('/users', userData).then(
			response => {
				dispatch(registerUserSuccess());
				dispatch(userLoggedOut());
			},
			error => {
				dispatch(registerUserFailure(error.response.data));
			}
		)
	}
};

const loginUserSuccess = (user, token) => {
	return {type: LOGIN_USER_SUCCESS, user, token};
};

const loginUserFailure = error => {
	return {type: LOGIN_USER_FAILURE, error};
};

export const loginUser = userData => {
	return dispatch => {
		return axios.post('/users/sessions', userData).then(
			response => {
				dispatch(loginUserSuccess(response.data.user, response.data.token));
				dispatch(userLoggedIn());
			},
			error => {
				const errorObj = error.response ? error.response.data : {error: 'No internet'};
				dispatch(loginUserFailure(errorObj));
			}
		)
	}
};

export const logoutUser = () => {
	return (dispatch) => {
		axios.delete('/users/sessions').then(
			response => {
				dispatch({type: LOGOUT_USER});
			}, error => {
				console.log('Logout Error');
			}
		);
	}
};

export const initApp = () => {
	return (dispatch, getState) => {
		const user = getState().user.user;
		if (!user) dispatch(userLoggedIn());
	}
};