import React, { Component } from 'react';
import { StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { PermissionsAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { CameraRoll } from "react-native";

class CameraScreen extends Component {
	
	state = {
		hasCameraPermissions: null,
		type: RNCamera.Constants.Type.back,
		flash: RNCamera.Constants.FlashMode.off,
		pictureTaken: false,
		picture: null
	};
	
	async componentWillMount() {
		
		try {
			const granted = await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.CAMERA,
				{
					'title': 'Cool Photo App Camera Permission',
					'message': 'Cool Photo App needs access to your camera ' +
					'so you can take awesome pictures.'
				}
			);
			
			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				console.log("You can use the camera");
				this.setState({hasCameraPermissions: granted});
				console.log('type' + RNCamera.Constants.Type.front);
			} else {
				console.log("Camera permission denied")
			}
		} catch (err) {
			console.warn(err)
		}
	};
	
	_changeType = () => {
		this.setState(prevState => {
			if (prevState.type === RNCamera.Constants.Type.back) {
				return {type: RNCamera.Constants.Type.front};
			} else {
				return {type: RNCamera.Constants.Type.back};
			}
		});
	};
	
	_changeFlash = () => {
		this.setState(prevState => {
			if (prevState.flash === RNCamera.Constants.FlashMode.off) {
				return {flash: RNCamera.Constants.FlashMode.on};
			} else if (prevState.flash === RNCamera.Constants.FlashMode.on) {
				return {flash: RNCamera.Constants.FlashMode.auto};
			} else if (prevState.flash === RNCamera.Constants.FlashMode.auto) {
				return {flash: RNCamera.Constants.FlashMode.off};
			}
		});
	};
	
	_takePhoto = async () => {
		const {pictureTaken} = this.state;
		if (!pictureTaken) {
			if (this.camera) {
				const takenPhoto = await this.camera.takePictureAsync({
					quality: 0.5,
					exif: true
				});
				this.setState({picture: takenPhoto.uri, pictureTaken: true});
			}
		}
	};
	
	_rejectPhoto = () => {
		this.setState({
			picture: null,
			pictureTaken: false
		});
	};
	_approvePhoto = async () => {
		const {picture} = this.state;
		const {navigation: {navigate}} = this.props;
		const saveResult = await CameraRoll.saveToCameraRoll(picture, "photo");
		navigate("UploadPhoto", {url: picture});
		this.setState({
			picture: null,
			pictureTaken: false
		});
	};
	
	render() {
		if (this.state.hasCameraPermissions === null) {
			return <View/>;
		} else if (this.state.hasCameraPermissions === false) {
			return <Text>No Access to Camera, check your settings</Text>;
		} else {
			return (
				<View style={styles.container}>
					<StatusBar hidden={true}/>
					{this.state.pictureTaken ? (
						<View style={{flex: 2}}>
							<FitImage source={{uri: this.state.picture}} style={{flex: 1}}/>
						</View>
					) : (
						<RNCamera
							type={this.state.type}
							flashMode={this.state.flash}
							ref={camera => (this.camera = camera)}
							style={styles.camera}
						>
							<TouchableOpacity onPressOut={this._changeType}>
								<View style={styles.action}>
									<Icon
										name={
											this.state.type === RNCamera.Constants.Type.back
												? "camera-front"
												: "camera-rear"
										}
										color="white"
										size={40}
									/>
								</View>
							</TouchableOpacity>
							<TouchableOpacity onPressOut={this._changeFlash}>
								<View style={styles.action}>
									{this.state.flash === RNCamera.Constants.FlashMode.off && (
										<Icon name={"flash-off"} color="white" size={40}/>
									)}
									{this.state.flash === RNCamera.Constants.FlashMode.on && (
										<Icon name={"flash-on"} color="white" size={40}/>
									)}
									{this.state.flash === RNCamera.Constants.FlashMode.auto && (
										<Icon
											name={"flash-auto"}
											color="white"
											size={40}
										/>
									)}
								</View>
							</TouchableOpacity>
						</RNCamera>
					)}
					<View style={styles.btnContainer}>
						{this.state.pictureTaken ? (
							<View style={styles.photoActions}>
								<TouchableOpacity onPressOut={this._rejectPhoto}>
									<Icon name={"cancel"} size={60} color="black"/>
								</TouchableOpacity>
								<TouchableOpacity onPressOut={this._approvePhoto}>
									<Icon
										name={"check-circle"}
										size={60}
										color="black"
									/>
								</TouchableOpacity>
							</View>
						) : (
							<TouchableOpacity onPressOut={this._takePhoto}>
								<View style={styles.btn}/>
							</TouchableOpacity>
						)}
					</View>
				</View>
			)
		}
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white"
	},
	camera: {
		flex: 2,
		alignItems: "flex-end",
		justifyContent: "space-between",
		flexDirection: "row"
	},
	btnContainer: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	btn: {
		width: 100,
		height: 100,
		backgroundColor: "white",
		borderColor: "#bbb",
		borderWidth: 15,
		borderRadius: 50
	},
	action: {
		backgroundColor: "transparent",
		height: 40,
		width: 40,
		margin: 10
	},
	photoActions: {
		flexDirection: "row",
		justifyContent: "space-around",
		flex: 1,
		alignItems: "center",
		width: 250
	}
});

export default CameraScreen;