import React, { Component } from "react";
import { CameraRoll } from "react-native";
import { View, StyleSheet, Dimensions, ScrollView, TouchableOpacity, Image } from "react-native";
import FitImage from "react-native-fit-image";
import Icon from 'react-native-vector-icons/MaterialIcons';

const {height, width} = Dimensions.get("window");

class LibraryScreen extends Component {
	state = {
		photos: null,
		pickedPhoto: null
	};
	
	async componentWillMount() {
		const {edges} = await CameraRoll.getPhotos({
			first: 200,
			assetType: "Photos"
		});
		
		this.setState({
			photos: edges,
			pickedPhoto: edges[0]
		})
	};
	
	render() {
		return (
			<View style={styles.container}>
				{this.props.photos && (
					<View style={styles.pictureContainer}>
						<FitImage
							source={{uri: this.props.pickedPhoto.node.image.uri}}
							style={styles.picture}
						/>
						<TouchableOpacity>
							<View style={styles.action}>
								<Icon name="check-circle" color="white" size={40}/>
							</View>
						</TouchableOpacity>
					</View>
				)}
				{this.props.photos && (
					<View style={styles.photos}>
						<ScrollView
							contentContainerStyle={styles.scrollViewContainer}
						>
							{this.props.photos.map((photo, index) => (
								<TouchableOpacity>
									<Image
										source={{uri: photo.node.image.uri}}
										style={styles.smallPhoto}
									/>
								</TouchableOpacity>))}
						</ScrollView>
					</View>
				)}
			</View>
		)
	};
	
	_pickPhoto = (photo) => {
		this.setState({
			pickedPhoto: photo
		})
	};
	
	_approvePhoto = () => {
		const {navigation: {navigate}} = this.this.props;
		const {pickedPhoto} = this.state;
		navigate("UploadPhoto", {
			url: pickedPhoto.node.image.uri
		});
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	pictureContainer: {
		flex: 2
	},
	picture: {
		width: width,
		height: width
	},
	photos: {
		flex: 1,
		alignItems: "stretch",
		justifyContent: "space-around",
	},
	scrollViewContainer: {
		flexDirection: "row",
		flexWrap: "wrap",
	},
	smallPhoto: {
		width: width / 4 - 1,
		height: width / 4,
		margin: StyleSheet.hairlineWidth
	},
	action: {
		backgroundColor: "transparent",
		height: 40,
		width: 40,
		alignSelf: "flex-end",
		position: "absolute",
		bottom: 10,
		right: 10
	}
});

export default LibraryScreen;