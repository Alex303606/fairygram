import React from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { initApp, loginUser } from "../../store/actions/user";
import {
	View,
	Text,
	StyleSheet,
	Dimensions,
	TouchableOpacity,
	TextInput,
	ActivityIndicator
} from "react-native";

const {width, height} = Dimensions.get("window");

class LoginScreen extends React.Component {
	state = {
		username: '',
		password: '',
		isSubmitting: false
	};
	
	componentDidMount() {
		setTimeout(() => this.props.initApp(), 1000);
	}
	
	submitFormHandler = () => {
		this.props.loginUser(this.state);
	};
	
	changeUsername = text => {
		this.setState({username: text});
	};
	
	changePassword = text => {
		this.setState({password: text});
	};
	
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<Text style={styles.text}>Login screen</Text>
				</View>
				<View style={styles.content}>
					{this.props.loginError && <Text style={styles.error}>{this.props.loginError.error}</Text>}
					<TextInput
						placeholder={"Username"}
						style={styles.textInput}
						autoCapitalize="none"
						autoCorrect={false}
						value={this.state.username}
						onChangeText={this.changeUsername}
					/>
					<TextInput
						placeholder={"Password"}
						style={styles.textInput}
						autoCapitalize="none"
						secureTextEntry={true}
						value={this.state.password}
						onChangeText={this.changePassword}
						returnKeyType={"send"}
						onSubmitEditing={this.submitFormHandler}
					/>
					<TouchableOpacity style={styles.touchable} onPressOut={this.submitFormHandler}>
						<View style={styles.button}>
							{this.state.isSubmitting ? (
								<ActivityIndicator size="small" color="white"/>
							) : (
								<Text style={styles.btnText}>Login</Text>
							)}
						</View>
					</TouchableOpacity>
					<TouchableOpacity style={styles.touchable}
					                  onPress={() => this.props.navigation.dispatch({type: 'Register'})}>
						<View style={styles.button}>
							<Text style={styles.btnText}>Register</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	header: {
		flex: 1,
		backgroundColor: "#4E65B4",
		alignItems: "center",
		justifyContent: "center",
		width
	},
	logo: {
		width: 180,
		height: 80,
		marginTop: 30
	},
	content: {
		flex: 4,
		backgroundColor: "white",
		paddingTop: 50,
		alignItems: "center",
		justifyContent: "flex-start"
	},
	textInput: {
		height: 50,
		borderColor: "#bbb",
		borderWidth: StyleSheet.hairlineWidth,
		width: width - 80,
		borderRadius: 5,
		marginBottom: 15,
		paddingHorizontal: 15,
		backgroundColor: "#FAFAFA",
		fontSize: 14
	},
	touchable: {
		borderRadius: 5,
		backgroundColor: "#3E99EE",
		width: width - 80,
		marginTop: 25
	},
	button: {
		paddingHorizontal: 7,
		height: 50,
		justifyContent: "center"
	},
	btnText: {
		color: "white",
		fontWeight: "600",
		textAlign: "center",
		fontSize: 14
	},
	text: {
		fontSize: 30,
		color: '#ffffff',
		fontWeight: 'bold'
	},
	error: {
		fontWeight: 'bold',
		color: 'red',
		fontSize: 20,
		paddingBottom: 20
	}
});

LoginScreen.propTypes = {
	navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
	loginError: state.user.loginError
});

const mapDispatchToProps = dispatch => ({
	loginUser: userData => dispatch(loginUser(userData)),
	initApp: () => dispatch(initApp())
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
