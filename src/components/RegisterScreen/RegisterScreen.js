import React from "react";
import { connect } from 'react-redux';
import {
	View,
	Text,
	StyleSheet,
	Dimensions,
	TouchableOpacity,
	TextInput,
	ActivityIndicator
} from "react-native";
import { registerUser } from "../../store/actions/user";
import PropTypes from "prop-types";
const {width, height} = Dimensions.get("window");

class RegisterScreen extends React.Component {
	
	state = {
		username: '',
		password: '',
		isSubmitting: false
	};
	
	submitFormHandler = () => {
		this.props.registerUser(this.state);
	};
	
	changeUsername = value => {
		this.setState({username: value});
	};
	
	changePassword = value => {
		this.setState({password: value});
	};
	
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<Text style={styles.text}>Register screen</Text>
				</View>
				<View style={styles.content}>
					{this.props.registerError && <Text style={styles.error}>{this.props.registerError.errors.username.message}</Text>}
					<TextInput
						placeholder={"Username"}
						style={styles.textInput}
						autoCapitalize="none"
						autoCorrect={false}
						value={this.state.username}
						onChangeText={this.changeUsername}
					/>
					<TextInput
						placeholder={"Password"}
						style={styles.textInput}
						autoCapitalize="none"
						secureTextEntry={true}
						value={this.state.password}
						onChangeText={this.changePassword}
						returnKeyType={"send"}
						onSubmitEditing={this.submitFormHandler}
					/>
					<TouchableOpacity style={styles.touchable} onPress={this.submitFormHandler}>
						<View style={styles.button}>
							{this.state.isSubmitting ? (
								<ActivityIndicator size="small" color="white"/>
							) : (
								<Text style={styles.btnText}>Register</Text>
							)}
						</View>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	header: {
		flex: 1,
		backgroundColor: "#b4b12c",
		alignItems: "center",
		justifyContent: "center",
		width
	},
	logo: {
		width: 180,
		height: 80,
		marginTop: 30
	},
	content: {
		flex: 4,
		backgroundColor: "white",
		paddingTop: 50,
		alignItems: "center",
		justifyContent: "flex-start"
	},
	textInput: {
		height: 50,
		borderColor: "#bbb",
		borderWidth: StyleSheet.hairlineWidth,
		width: width - 80,
		borderRadius: 5,
		marginBottom: 15,
		paddingHorizontal: 15,
		backgroundColor: "#FAFAFA",
		fontSize: 14
	},
	touchable: {
		borderRadius: 5,
		backgroundColor: "#3E99EE",
		width: width - 80,
		marginTop: 25
	},
	button: {
		paddingHorizontal: 7,
		height: 50,
		justifyContent: "center"
	},
	btnText: {
		color: "white",
		fontWeight: "600",
		textAlign: "center",
		fontSize: 14
	},
	text: {
		fontSize: 30,
		color: '#ffffff',
		fontWeight: 'bold'
	},
	error: {
		fontWeight: 'bold',
		color: 'red',
		fontSize: 20,
		paddingBottom: 20
	}
});

RegisterScreen.propTypes = {
	navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
	registerError: state.user.registerError,
	nav: state.nav
});

const mapDispatchToProps = dispatch => ({
	registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);
