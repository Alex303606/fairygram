import { createBottomTabNavigator } from "react-navigation";
import CameraScreen from "../screens/CameraScreen/CameraScreen";
import LibraryScreen from "../screens/LibraryScreen/LibraryScreen";

const AddPhotoNavigation = createBottomTabNavigator(
	{
		Camera: {
			screen: CameraScreen,
			path: 'CameraScreen',
			naviagtionOptions: {
				tabBarLabel: "Photo",
			}
		},
		Library: {
			screen: LibraryScreen,
			naviagtionOptions: {
				tabBarLabel: "Library"
			}
		}
	},
	{
		tabBarPosition: "top",
		swipeEnabled: true,
		animationEnabled: true,
		tabBarOptions: {
			showLabel: true,
			upperCaseLabel: true,
			activeTintColor: "black",
			inactiveTintColor: "#bbb",
			style: {
				backgroundColor: "white",
				alignItems: "center"
			},
			labelStyle: {
				fontSize: 14,
				fontWeight: "600"
			},
			showIcon: false
		}
	},
	{
		initialRouteName: 'Camera'
	}
);

export default AddPhotoNavigation;
