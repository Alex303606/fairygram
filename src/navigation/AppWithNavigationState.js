import React from 'react';
import { connect } from 'react-redux';
import { createSwitchNavigator } from 'react-navigation';
import { initializeListeners } from 'react-navigation-redux-helpers';
import { navigationPropConstructor } from '../utils/redux';
import PropTypes from 'prop-types';
import RootNavigation from "./RootNavigation";
import AuthNavigator from "./AuthNavigator";
import { Platform, BackHandler } from "react-native";

export const AppNavigator = createSwitchNavigator(
	{
		Auth: {
			screen: AuthNavigator,
			navigationOptions: {
				header: null
			}
		},
		Home: {
			screen: RootNavigation,
			navigationOptions: {
				header: null
			}
		}
	},
	{
		initialRouteName: 'Auth'
	}
);

class AppWithNavigationState extends React.Component {
	static propTypes = {
		dispatch: PropTypes.func.isRequired,
		nav: PropTypes.object.isRequired,
	};
	
	componentWillMount() {
		if (Platform.OS !== 'android') return;
		BackHandler.addEventListener('hardwareBackPress', () => {
			const {dispatch} = this.props;
			dispatch({type: 'Back'});
			return true;
		})
	}
	
	componentWillUnmount() {
		if (Platform.OS === 'android') BackHandler.removeEventListener('hardwareBackPress');
	}
	
	componentDidMount() {
		initializeListeners('root', this.props.nav);
	}
	
	render() {
		const {dispatch, nav} = this.props;
		const navigation = navigationPropConstructor(dispatch, nav);
		return <AppNavigator navigation={navigation}/>;
	}
}

const mapStateToProps = state => ({
	nav: state.nav
});

export default connect(mapStateToProps)(AppWithNavigationState);