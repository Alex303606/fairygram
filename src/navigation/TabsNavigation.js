import React from "react";
import { View } from "react-native";
import { createBottomTabNavigator } from "react-navigation";
import HomeRoute from "../routes/HomeRoute";
import SearchRoute from "../routes/SearchRoute";
import NotificationsRoute from "../routes/NotificationsRoute";
import ProfileRoute from "../routes/ProfileRoute";
import Icon from 'react-native-vector-icons/Ionicons';

const TabsNavigation = createBottomTabNavigator(
	{
		Home: {
			screen: HomeRoute,
			navigationOptions: {
				tabBarIcon: ({focused}) => (
					<Icon
						name={focused ? "ios-home" : "ios-home-outline"}
						size={30}
						color={"black"}
					/>
				)
			}
		},
		Search: {
			screen: SearchRoute,
			navigationOptions: {
				tabBarIcon: ({focused}) => (
					<Icon
						name={focused ? "ios-search" : "ios-search-outline"}
						size={30}
						color={"black"}
					/>
				)
			}
		},
		AddPhoto: {
			screen: View,
			navigationOptions: {
				tabBarIcon: ({focused}) => (
					<Icon name={"ios-add-circle-outline"} size={30} color={"black"}/>
				)
			}
		},
		Notifications: {
			screen: NotificationsRoute,
			navigationOptions: {
				tabBarIcon: ({focused}) => (
					<Icon
						name={focused ? "ios-heart" : "ios-heart-outline"}
						size={30}
						color={"black"}
					/>
				)
			}
		},
		Profile: {
			screen: ProfileRoute,
			navigationOptions: {
				tabBarIcon: ({focused}) => (
					<Icon
						name={focused ? "ios-person" : "ios-person-outline"}
						size={30}
						color={"black"}
					/>
				)
			}
		}
	},
	{
		initialRouteName: 'Home'
	}
);

export default TabsNavigation;