import React from "react";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import AppWithNavigationState from "./navigation/AppWithNavigationState";

class App extends React.Component {
	render() {
		return (
			<Provider store={configureStore}>
				<AppWithNavigationState/>
			</Provider>
		);
	}
}

export default App;